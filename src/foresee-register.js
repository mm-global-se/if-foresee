mmcore.IntegrationFactory.register(
  'foresee', {
   validate: function (data) {  
        if(!data.campaign)
            return 'No campaign.';
        return true;  
    },
    check: function (data) {  
      return window.FSR && window.FSR.CPPS && window.FSR.CPPS.set; 
    },
    exec: function (data) {
         var prodSand = data.isProduction ? 'MM_Prod_' : 'MM_QA_';
         FSR.CPPS.set( prodSand + data.campaign, data.campaignInfo);  
        if (data.callback) data.callback();
        return true;
    }  
});
