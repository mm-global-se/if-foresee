# Foresee

---

[TOC]

## Overview
This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to Foresee.

###How we send the data
Our Foresee integration stores the campaign data that each user is exposed to in the `fsr.s` cookie.


###Data Format 
The data sent to Foresee will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
) 

By default on each page the data is concatenated for multiple campaigns so to stop each campaign data replacing previous data that was sent.

##Prerequisite
The following information needs to be provided by the client: 

+ Campaign Name


## Download

* [foresee-register.js](https://bitbucket.org/mm-global-se/if-foresee/src/master/src/foresee-register.js)

* [foresee-initialize.js](https://bitbucket.org/mm-global-se/if-foresee/src/master/src/foresee-initialize.js)


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [foresee-register.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/foresee-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Foresee Register scripts!

+ Create a campaign script and add the [foresee-initialize.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/foresee-initialize.js) script. Customise the code by changing the campaign name. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-foresee#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

###Dev Console

+ Run the following line in the console of the browser. It should return with the list of campaign data that is sent to Foresee.

`JSON.parse(mmcore.GetCookie('fsr.s',1)).cp`
 
## Common Problem Solutions

+ None Currently Known